package ru.codeinside.lessons.javacore.exceptionlab;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

import static java.lang.String.format;

public class OperationFactory {

    private final Logger log = LogManager.getLogger(OperationFactory.class);

    public static void main(String[] args) {
        var operationFactory = new OperationFactory();
        operationFactory.parseAndDivide();
        operationFactory.checkLength();
        operationFactory.getFinalInfo();
    }

    private void getFinalInfo() {
        log.info("Проверка окончена");
    }

    private void parseAndDivide() {
        log.info("метод parseAndDivide() успешно стартовал.\n");
        List<String> source = List.of("2", "5", "0", "10", "10000000000", "-100", "qwerty");
        for (String element : source) {
            try {
                int value = Integer.parseInt(element);
                double result = 1000 / value;
                System.out.println(result);
            } catch (ArithmeticException e) {
                log.error("Деление на нуль\n");
            } catch (NumberFormatException e) {
                log.error(format("Неправильный формат числа '%s'\n", element));
            }
        }
    }

    private void checkLength() {
        List<String> source = Arrays.asList("car", "table", "", "01", "alphabet", null, "zero");
        for (String element : source) {
            try {
                int length = element.length();
                System.out.printf("Длина переданного слова '%s': %d \n", element, length);
            } catch (NullPointerException e) {
                log.error("Переданный объект - null\n");
            }
        }
    }
}
